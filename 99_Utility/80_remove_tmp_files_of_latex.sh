#!/bin/sh

if [ $# -ne 1 ] ; then
  echo "only one argument is required. Target Directory"
else
  if [ -d $1 ] ; then
    for ff in aux dvi log ; do
      find $1 -type f -name "*."$ff -exec echo {}" will be removed" \;
      find $1 -type f -name "*."$ff -exec rm {} \; 
      echo ""
    done
  else
    echo "Target Directory is not existed"
  fi
fi
